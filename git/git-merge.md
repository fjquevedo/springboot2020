# GIT ESTRATEGIAS DE CONSOLIDACIÓN
Estrategias y comandos.

## GIT MERGE
Crea un nuevo commit en la integración con la rama origen.
PROS:
    conserva historia commits
CONS:
    historial complicado

## GIT REBASE
A partir de nuestra rama (feature) cambia el origen de nuestra rama (base) a el último commit de la rama destino (master/develop).
Cambiar la base implica coger los hash de commits y reemplazarlos por otros nuevos. Los lleva temporalmente a holding-area y luego cambia los commits.
Si se hace rebase sobre commits que están en origin, cambiará los commits complicará trabajar en la misma rama con otros compañeros.

En caso que haya muchos integraciónes de merge develop -> feature, en procesos largos de desarrollo. Se puede plantear un Rebase previo a a la integración con develop para luego hacer fast-forward.

PROS:
    permite un historial limpio de commits
    historial lineal
CONS:
    reduce la trazabilidad.

## GIT SQUASH

PROS:
    historial conciso y limpio
CONS:
    perdida de detalles commit
    reduce la trazabilidad.


## RECURSOS
https://www.youtube.com/watch?v=HlmZLXMOpEM
https://www.youtube.com/watch?v=zOnwgxiC0OA
https://www.youtube.com/watch?v=0chZFIZLR_0

https://stackoverflow.com/questions/9069061/what-effect-does-the-no-ff-flag-have-for-git-merge
https://nvie.com/posts/a-successful-git-branching-model/





Git Merge
Es una operación que combina dos ramas diferentes. Al ejecutar git merge, Git intenta fusionar los cambios de la rama de destino con la rama activa. Si hay conflictos, Git te pedirá que los resuelvas manualmente antes de completar el merge. Es una herramienta común para integrar características desarrolladas en ramas separadas.

Git Rebase
Rebase es una forma de mover o reordenar commits. git rebase se utiliza para aplicar cambios desde una rama base a la rama actual de una forma más lineal. Reescribe el historial de commits al mover los commits de la rama actual al final de la rama base seleccionada, eliminando la complejidad del historial. Rebase también se puede utilizar para aplicar commits de una rama diferente, lo que se conoce como rebase interactivo.

Git Squash
Esta técnica permite combinar varios commits en uno solo para simplificar el historial del proyecto. Se usa típicamente con rebase interactivo (git rebase -i). Puedes marcar commits específicos para ser "squashed", es decir, combinados en un solo commit con un mensaje descriptivo, facilitando un historial más limpio.

Git Cherry-pick
git cherry-pick te permite aplicar un commit específico desde una rama a otra sin fusionar la rama completa. Esto es útil para tomar cambios específicos de una rama y aplicarlos a otra, sin arrastrar todos los cambios asociados. Se usa mucho para aplicar correcciones o mejoras puntuales en distintas ramas.

En resumen:

git merge: combina el historial de dos ramas.
git rebase: mueve commits para reordenar el historial.
git squash: combina múltiples commits en uno.
git cherry-pick: aplica un commit específico de una rama a otra.

## pro-cons

Git Merge
Pros:

Historial completo: El historial mantiene toda la información sobre cuándo se fusionaron las ramas y su contexto.
Integración sencilla: Para proyectos grandes, permite fusionar ramas de forma sencilla sin alterar los commits ya realizados.
Conflictos manejables: Los conflictos pueden ser más fáciles de identificar y resolver, ya que el historial no se reescribe.
Contras:

Historial más complejo: Puede resultar en un historial más enmarañado con múltiples ramas.
Commits de merge innecesarios: Puede generar commits de merge adicionales que no aportan cambios de código.
Git Rebase
Pros:

Historial más limpio: Crea un historial lineal, facilitando la comprensión del historial del proyecto.
Simplifica la revisión: Al evitar merges complejos, la revisión de cambios puede ser más directa.
Contras:

Historial reescrito: Modifica el historial existente, lo que puede causar problemas si varios colaboradores ya han usado esos commits.
Conflictos más difíciles: Puede ser más complicado resolver conflictos ya que el contexto de los cambios puede haberse perdido.
Git Squash
Pros:

Historial limpio: Agrupa múltiples commits pequeños en un solo commit significativo.
Mejora la legibilidad: Facilita la comprensión del historial al reducir el número de commits.
Contras:

Pérdida de granularidad: Puede perderse el detalle de los cambios individuales realizados.
Modificación de commits anteriores: Como rebase, puede alterar el historial si se ha compartido con otros colaboradores.
Git Cherry-pick
Pros:

Aplicación puntual: Permite aplicar cambios específicos sin fusionar ramas enteras.
Flexibilidad: Ofrece flexibilidad para mover commits entre ramas.
Contras:

Conflictos inesperados: Puede introducir conflictos inesperados si el commit tiene dependencias no obvias.
Historial duplicado: Puede generar duplicados en el historial si se cherry-pickea un commit que luego se fusiona por otra ruta.



# gitflow

Gitflow es una metodología de ramificación que facilita la colaboración en el desarrollo de software. Aquí tienes cómo puedes gestionar el flujo de cambios entre las ramas feature, develop, release y master usando Merge Requests (MR) en GitLab o Pull Requests (PR) en GitHub:

Integrar cambios de feature a develop
Finaliza el desarrollo: Completa el desarrollo en la rama feature.
Crea un Merge Request (MR): Desde la rama feature, abre un MR hacia la rama develop.
Revisión y aprobación: Pide a tus compañeros de equipo que revisen los cambios.
Merge: Una vez aprobado, fusiona el MR. Esto integrará los cambios en develop.
Crear una rama release y trabajar en correcciones
Crea la rama release: A partir de develop, crea una nueva rama llamada release/x.y (x.y es la versión).
bash
Copy code
git checkout develop
git checkout -b release/x.y
Haz correcciones en release: Realiza correcciones directamente en la rama release.
Crear MRs de release a develop: Cada corrección puede requerir un MR para aplicar los cambios de release a develop. Si los cambios son pequeños, puedes hacer esto manualmente sin un MR.
Integrar release a master
Finalizar el desarrollo: Asegúrate de que todos los cambios en release están listos.
Merge a master: Fusiona la rama release/x.y a master para publicar la nueva versión. Esto podría hacerse mediante un MR desde release/x.y a master.
Etiqueta la versión: Etiqueta el commit en master con el número de versión para marcar la publicación.
bash
Copy code
git checkout master
git tag -a vX.Y -m "Version X.Y"
git push origin vX.Y
Merge a develop: Fusiona la rama release/x.y nuevamente a develop para mantenerlo sincronizado con master.
bash
Copy code
git checkout develop
git merge release/x.y
Siguiendo estos pasos, mantendrás un flujo claro de cambios entre las ramas en el contexto de Gitflow.


##
en la rama release trabajan varias personas.
es decir que constantemente sacan ramas fix/xxx luego hacer merge a release.
sería recomendable luego hacer merge desde release a develop? o desfe fix/xxx a develop?
ChatGPT
Cuando trabajas en un flujo de trabajo donde varias personas realizan correcciones (fix/xxx) en una rama release, y esos cambios deben ser fusionados a develop, es importante considerar el flujo de los cambios para mantener un historial limpio y evitar conflictos. Aquí tienes algunas consideraciones:

Merge desde release a develop
Pros:

Historial centralizado: Todos los cambios se integran a develop desde una única rama (release), lo que hace que el historial sea más limpio y fácil de seguir.
Menos esfuerzo: Evita el esfuerzo manual de fusionar cada cambio individualmente desde las ramas fix/xxx.
Contras:

Conflictos potenciales: Si hay cambios significativos en develop que no están presentes en release, pueden ocurrir conflictos cuando se hace merge de release.
Historial complejo: El historial de cambios puede ser más difícil de entender debido a que todos los cambios se fusionan desde release a develop.
Merge desde fix/xxx a develop
Pros:

Menos conflictos: Dado que los cambios se integran a develop directamente, es menos probable que se produzcan conflictos.
Historial granular: El historial de develop reflejará los cambios individuales, proporcionando más contexto sobre cada cambio.
Contras:

Esfuerzo adicional: Requiere que cada rama fix/xxx se fusione individualmente a develop.
Duplicación de trabajo: Puede ser redundante si luego todos estos cambios se fusionan también desde release a develop.
Recomendación General
Si la rama release se usa para preparar una nueva versión estable que luego se fusiona a master, lo mejor es fusionar release a develop para mantener el historial sincronizado.
Si necesitas que los cambios se reflejen en develop más rápidamente (antes de la integración final de release), considera fusionar cada fix/xxx directamente a develop.
En la práctica, el enfoque más adecuado dependerá del flujo de trabajo de tu equipo y de la importancia de reflejar los cambios en develop en tiempo real.