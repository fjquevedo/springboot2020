package com.minsait.onesait.customer.lite.commons.storage.reader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import com.minsait.onesait.architecture.multitenant.context.MultiTenantContext;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import io.minio.Result;
import io.minio.messages.Item;

@Component
@ConditionalOnProperty(value = { "customer.storage.type" }, havingValue = "FILE_SYSTEM", matchIfMissing = true)
@Primary
public class FileSystemReader implements Reader {

  private static final String NOT_IMPLEMENTED = "Método no implementado";
  @Value("${customer.storage.prefix:#{null}}")
  private Optional<String> prefix;

  @Override
  public byte[] read(String filePath) throws StorageException {
    Path pathFinal = concatBaseDir(filePath);

    try (InputStream inputStream = Files.newInputStream(Path.of(pathFinal.toString()))) {
      return inputStream.readAllBytes();
    } catch (IOException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }

  @Override
  public byte[] read(String filePath, String prefix) throws StorageException {
    Path pathFinal = concatArgDir(filePath, prefix);
    try (InputStream inputStream = Files.newInputStream(Path.of(pathFinal.toString()))) {
      return inputStream.readAllBytes();
    } catch (IOException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }

  @Override
  public byte[] readByBucket(String bucketName, String filePath) throws StorageException {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  @Override
  public InputStream readInputStream(String filePath) throws StorageException {
    Path pathFinal = concatBaseDir(filePath);

    try {
      return new FileInputStream(pathFinal.toString());
    } catch (FileNotFoundException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }

  @Override
  public List<String> listFiles(String filePath) throws StorageException {

    Path directorioPath = concatBaseDir(filePath);

    // Inicializar el flujo de archivos
    Stream<Path> archivosStream = null;

    try {
      // Verificar si el directorio existe y es un directorio
      if (Files.isDirectory(directorioPath)) {
        archivosStream = Files.list(directorioPath)
            .filter(Files::isRegularFile)
            .map(file -> directorioPath.relativize(file).toString())
            .map(relativePath -> directorioPath.subpath(1, directorioPath.getNameCount()).resolve(relativePath));

        // Utilizar el flujo para obtener los nombres de archivo
        return archivosStream
            .map(Path::toString)
            .toList();

      } else {
        return new ArrayList<>();

      }
    } catch (IOException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);

    } finally {
      if (archivosStream != null) {
        archivosStream.close();
      }
    }
  }

  @Override
  public List<Result<Item>> searchFilesByTags(Map<String, String> tags) {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  @Override
  public String generateLink(String objectName, int days) throws StorageException {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  @Override
  public String generateLink(String filePath, int days, boolean isObfuscated) throws StorageException {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  private Path concatBaseDir(String path) {
    String baseDir;

    if (prefix.isPresent() && !prefix.get().isEmpty()) {
      baseDir = "/" + prefix.get() + "-" + MultiTenantContext.getTenantId();
    } else {
      baseDir = "/" + MultiTenantContext.getTenantId();
    }

    baseDir = baseDir.toLowerCase();

    String fullPath = StringUtils.startsWith(path, "/") ? path : "/".concat(path);
    return Paths.get(baseDir, fullPath);
  }

  private Path concatArgDir(String path, String prefix) {
    String baseDir = "/" + prefix + "-" + MultiTenantContext.getTenantId();
    baseDir = baseDir.toLowerCase();

    String fullPath = StringUtils.startsWith(path, "/") ? path : "/".concat(path);
    return Paths.get(baseDir, fullPath);
  }

  @Override
  public void downloadLink(String etag, HttpServletRequest request, HttpServletResponse response)
      throws StorageException {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  @Override
  public List<String> listFiles() throws StorageException {

    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  @Override
  public List<String> listFiles(String filePath, boolean isRecursive) throws StorageException {
    List<String> listFiles = new ArrayList<>();

    Path path = concatBaseDir(filePath);

    if (!Files.exists(path)) {
      return listFiles;
    }

    try (Stream<Path> pathStream = Files.walk(path, Integer.MAX_VALUE, FileVisitOption.FOLLOW_LINKS)) {
      pathStream
          .filter(Files::isRegularFile)
          .map(file -> path.relativize(file).toString())
          .map(relativePath -> path.subpath(1, path.getNameCount()).resolve(relativePath).toString())

          .forEach(listFiles::add);
    } catch (IOException e) {
      throw new StorageException("Error al listar archivos.", e);
    }
    return listFiles;

  }

  @Override
  public boolean fileExist(String filePath) throws StorageException {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }
}
