package com.minsait.onesait.customer.lite.commons.storage.reader;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.minsait.onesait.architecture.multitenant.context.MultiTenantContext;
import com.minsait.onesait.customer.lite.commons.storage.exception.FTPException;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import com.minsait.onesait.customer.lite.commons.storage.ftp.FTPClientAutoCloseable;
import com.minsait.onesait.customer.lite.commons.storage.ftp.FTPConnectionPoolManager;
import io.minio.Result;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Qualifier("ftpReader")
public class FTPReaderImpl implements FTPReader {

  @Autowired
  private FTPConnectionPoolManager ftpPoolManager;

  @Override
  public List<String> listFilesGT(String path, Duration time) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();

    List<String> fileList = new ArrayList<>();

    try (FTPClientAutoCloseable ftpClientAutoCloseable = ftpPoolManager.getConnection(tenantId)) {
      FTPClient ftpClient = ftpClientAutoCloseable.getClient();

      FTPFile[] files;
      try {
        files = ftpClient.listFiles(path);
      } catch (IOException ex) {
        throw new StorageException("Exception getting remote files", ex);
      }
      for (FTPFile file : files) {
        if (file.isFile()) {
          Instant fileModifiedTime = file.getTimestamp().toInstant();
          Duration fileAge = Duration.between(fileModifiedTime, Instant.now());
          if (fileAge.compareTo(time) > 0) {
            fileList.add(file.getName());
          }
        }
      }
    } catch (FTPException ex) {
      throw new StorageException("Exception getting ftp connection", ex);
    }

    return fileList;

  }

  @Override
  public byte[] read(String filePath) throws StorageException {
    throw new UnsupportedOperationException("");
  }

  @Override
  public byte[] read(String filePath, String prefix) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public byte[] readByBucket(String bucketName, String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public InputStream readInputStream(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> listFiles() throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<Result<Item>> searchFilesByTags(Map<String, String> tags) {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public String generateLink(String filePath, int days) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public String generateLink(String filePath, int days, boolean isObfuscated) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void downloadLink(String etag, HttpServletRequest request, HttpServletResponse response)
      throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> listFiles(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> listFiles(String filePath, boolean isRecursive) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public boolean fileExist(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }



}

