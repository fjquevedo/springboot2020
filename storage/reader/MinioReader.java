package com.minsait.onesait.customer.lite.commons.storage.reader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.minsait.onesait.customer.lite.commons.storage.commons.MinioCommon;
import com.minsait.onesait.customer.lite.commons.storage.entity.Route;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import com.minsait.onesait.customer.lite.commons.storage.interceptor.PrefixHolder;
import com.minsait.onesait.customer.lite.commons.storage.repository.RouteRepository;
import com.minsait.onesait.customer.lite.commons.storage.utils.MapUtils;
import io.minio.GetObjectArgs;
import io.minio.GetObjectTagsArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.ListObjectsArgs;
import io.minio.Result;
import io.minio.StatObjectArgs;
import io.minio.StatObjectResponse;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.MinioException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import io.minio.http.Method;
import io.minio.messages.Item;
import io.minio.messages.Tags;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@ConditionalOnProperty(value = {"customer.storage.type"}, havingValue = "MINIO")
@Primary
public class MinioReader extends MinioCommon implements Reader {

  private final RestTemplate restTemplate;

  @Value("${server.servlet.context-path:#{null}}")
  private Optional<String> contextPath;

  @Value("${clite.host:#{null}}")
  private Optional<String> host;

  private final RouteRepository repository;

  public MinioReader(@Value("${customer.storage.accessKey}") String accessKey,
      @Value("${customer.storage.secretKey}") String secretKey,
      @Value("${customer.storage.endpoint}") String endpoint,
      PrefixHolder prefixHolder,
      RestTemplate restTemplate,
      RouteRepository repository) {
    super(accessKey, secretKey, endpoint, prefixHolder);
    this.restTemplate = restTemplate;
    this.repository = repository;
  }

  @Override
  public byte[] read(String objectName) throws StorageException {
    String bucketName = getBucketName();
    try (InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
        .bucket(bucketName)
        .object(objectName)
        .build())) {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      byte[] buffer = new byte[4096];
      int len;
      while ((len = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, len);
      }

      return outputStream.toByteArray();
    } catch (MinioException | IOException | InvalidKeyException | NoSuchAlgorithmException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }


  @Override
  public byte[] read(String objectName, String prefix) throws StorageException {
    String bucketName = getArgBucketName(prefix);
    try (InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
        .bucket(bucketName)
        .object(objectName)
        .build())) {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      byte[] buffer = new byte[4096];
      int len;
      while ((len = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, len);
      }

      return outputStream.toByteArray();
    } catch (MinioException | IOException | InvalidKeyException | NoSuchAlgorithmException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }

  /**
   * GAFEGA-121415 Get image from a specific bucket
   * 
   * @param bucketName String bucket name
   * @param filePath String path file
   * @return byte[]
   * @throws StorageException storage exception
   */
  @Override
  public byte[] readByBucket(String bucketName, String filePath) throws StorageException {
    try (InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
        .bucket(bucketName)
        .object(filePath)
        .build())) {
      var outputStream = new ByteArrayOutputStream();
      var buffer = new byte[4096];
      int len;
      while ((len = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, len);
      }

      return outputStream.toByteArray();
    } catch (MinioException | IOException | InvalidKeyException | NoSuchAlgorithmException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }

  @Override
  public InputStream readInputStream(String objectName) throws StorageException {
    String bucketName = getBucketName();

    try {
      return minioClient.getObject(GetObjectArgs.builder()
          .bucket(bucketName)
          .object(objectName)
          .build());
    } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException(Reader.ERR_READING_FILE, e);
    }
  }

  @Override
  public List<String> listFiles() throws StorageException {
    List<String> objectNames = new ArrayList<>();

    try {
      String bucketName = getBucketName();
      Iterable<Result<Item>> objects = minioClient.listObjects(ListObjectsArgs.builder()
          .bucket(bucketName)
          .recursive(true)
          .build());

      for (Result<Item> result : objects) {
        objectNames.add(result.get().objectName());
      }
    } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException(
          "Se produjo un error al listar objetos del bucket. Detalles del error: " + e.getMessage(), e);
    }

    return objectNames;
  }

  @Override
  public List<Result<Item>> searchFilesByTags(Map<String, String> tags) {
    String bucketName = getBucketName();

    ListObjectsArgs args = ListObjectsArgs.builder()
        .bucket(bucketName)
        .recursive(true)
        .build();
    Iterable<Result<Item>> objects = minioClient.listObjects(args);

    return StreamSupport.stream(objects.spliterator(), false)
        .filter(obj -> {
          try {
            Item item = obj.get();
            Tags objectTags = minioClient.getObjectTags(
                GetObjectTagsArgs.builder()
                    .bucket(bucketName)
                    .object(item.objectName())
                    .build());
            return MapUtils.checkMaps(tags, objectTags.get());
          } catch (Exception e) {
            log.error("Exception {}", e.getMessage());
            return false;
          }
        }).collect(Collectors.toList());
  }

  @Override
  public String generateLink(String objectName, int days) throws StorageException {
    return generateLink(objectName, days, true);
  }

  @Override
  public String generateLink(String objectName, int days, boolean isObfuscated) throws StorageException {
    try {
      // Limit the duration to a maximum of 7 days
      int duration = Integer.min(days, 7);
      TimeUnit unit = TimeUnit.DAYS;

      // Get the bucket name
      String bucketName = getBucketName();

      // Generate the original presigned URL
      String originalUrl = URLDecoder.decode(
          minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
              .method(Method.GET)
              .bucket(bucketName)
              .object(objectName)
              .expiry(duration, unit)
              .build()),
          StandardCharsets.UTF_8);

      if (isObfuscated) {
        // If obfuscated, create a new URL with the object's etag as part of the path
        StatObjectResponse response = minioClient.statObject(StatObjectArgs.builder()
            .bucket(bucketName)
            .object(objectName)
            .build());
        String etag = response.etag();

        String path = String.format("%s/download/%s", contextPath.orElseThrow(), etag);
        String newUrl = new URI("https", host.orElseThrow(), path, null)
            .toURL()
            .toString();

        // Calculate the expiration dates for the URL and MinIO
        Instant nowDate = Instant.now();
        String expiry = nowDate.plusNanos(unit.toNanos(days))
            .toString();
        String minioExpiry = nowDate.plusNanos(unit.toNanos(duration))
            .toString();

        // Check if the route already exists in the repository
        Optional<Route> routeOpt = repository.findByEtag(etag);
        Route route;
        if (routeOpt.isPresent()) {
          // If the route exists, update its URL, MinIO expiry, and URL expiry
          route = routeOpt.get();
          route.setUrl(originalUrl);
          route.setMinioExpiry(minioExpiry);
          route.setExpiry(expiry);
        } else {
          // If the route doesn't exist, create a new Route object
          route = Route.builder()
              .etag(etag)
              .bucketName(bucketName)
              .objectName(objectName)
              .url(originalUrl)
              .expiry(expiry)
              .minioExpiry(minioExpiry)
              .build();
        }
        // Save the route in the repository
        repository.save(route);

        return newUrl;
      } else {
        // If not obfuscated, return the original presigned URL
        return originalUrl;
      }
    } catch (Exception e) {
      throw new StorageException("Error al obtener el enlace de descarga pre-firmado: " + e.getMessage(), e);
    }
  }

  @Override
  public void downloadLink(String etag, HttpServletRequest request, HttpServletResponse response)
      throws StorageException {
    try {
      // Find the route in the repository using the etag
      Optional<Route> routeOpt = repository.findByEtag(etag);
      Route route = routeOpt.orElseThrow();

      // Parse the expiry dates from strings to Instant objects
      Instant expiryDate = Instant.parse(route.getExpiry());
      Instant minioExpiryDate = Instant.parse(route.getMinioExpiry());
      Instant nowDate = Instant.now();

      // Calculate the remaining time until the expiry dates
      Duration diffExpiry = Duration.between(nowDate, expiryDate);
      Duration diffMinioExpiry = Duration.between(nowDate, minioExpiryDate);

      // Check if the presigned URL has expired
      if (diffExpiry.isNegative()) {
        throw new StorageException("La URL presignada ha expirado", null);
      }

      // Get the original URL from the route
      String url = route.getUrl();

      // Check if the MinIO presigned URL has expired
      if (diffMinioExpiry.isNegative()) {
        // Calculate the remaining days until the URL expiry
        int daysRemaining = Integer.min((int) diffExpiry.toDays(), 7);

        // Generate a new MinIO presigned URL with the remaining days
        url = URLDecoder.decode(minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
            .method(Method.GET)
            .bucket(route.getBucketName())
            .object(route.getObjectName())
            .expiry(daysRemaining, TimeUnit.DAYS)
            .build()), StandardCharsets.UTF_8);

        // Update the route with the new URL and save it to the repository
        route.setUrl(url);
        route.setMinioExpiry(minioExpiryDate.toString());
        repository.save(route);
      }

      // Download the object from the URL using ResponseEntity
      ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, byte[].class);

      // Set response headers
      HttpHeaders headers = responseEntity.getHeaders();
      headers.forEach((header, value) -> response.addHeader(header, value.get(0)));

      // Get content type and response body
      MediaType contentType = headers.getContentType();
      byte[] responseBody = responseEntity.getBody();

      // Write the response to the output stream
      if (responseBody != null && contentType != null) {
        response.setContentType(contentType.toString());
        response.getOutputStream().write(responseBody);
      }
    } catch (Exception e) {
      throw new StorageException("Error al intentar ofuscar y descargar de MinIO", e);
    }
  }

  @Override
  public List<String> listFiles(String filePath, boolean isRecursive) throws StorageException {
    List<String> files = new ArrayList<>();

    String bucketName = getBucketName();

    try {
      Iterable<Result<Item>> results = minioClient.listObjects(
          ListObjectsArgs.builder()
              .bucket(bucketName)
              .prefix(filePath)
              .recursive(isRecursive)
              .build());

      for (Result<Item> result : results) {
        Item item = result.get();
        String objectName = item.objectName();

        files.add(objectName);

      }
    } catch (Exception e) {
      throw new StorageException("Error al intentar ofuscar y descargar de MinIO", e);
    }

    return files;
  }

  @Override
  public List<String> listFiles(String filePath) throws StorageException {

    return listFiles(filePath, false);
  }

  @Override
  public boolean fileExist(String filePath) throws StorageException {

    String bucketName = getBucketName();


    try {
      StatObjectResponse file = minioClient.statObject(StatObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .build());


      return file != null;
    } catch (InvalidKeyException | ErrorResponseException | InsufficientDataException | InternalException
        | InvalidResponseException | NoSuchAlgorithmException | ServerException | XmlParserException
        | IllegalArgumentException | IOException e) {
      throw new StorageException("Error al leer el archivo", e);

    }

  }
}
