package com.minsait.onesait.customer.lite.commons.storage.reader;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.stereotype.Component;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.minsait.onesait.architecture.multitenant.context.MultiTenantContext;
import com.minsait.onesait.customer.lite.commons.storage.exception.FTPException;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import com.minsait.onesait.customer.lite.commons.storage.ftp.FTPClientAutoCloseable;
import com.minsait.onesait.customer.lite.commons.storage.ftp.FTPConnectionPoolManager;
import com.minsait.onesait.customer.lite.commons.storage.sftp.SFTPTenantConfiguration;
import io.minio.Result;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Qualifier("sftpReader")
public class SFTPReaderImpl implements FTPReader {

  @Autowired
  private SFTPTenantConfiguration multitenantConfig;

  @Override
  public List<String> listFilesGT(String path, Duration time) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();

    SessionFactory<LsEntry> sessionFactoryForTenant = multitenantConfig.getSessionFactoryForTenant(tenantId);
    SftpRemoteFileTemplate template = new SftpRemoteFileTemplate(sessionFactoryForTenant);

    return template.execute(session -> {
      LsEntry[] entries = session.list(path);
      return Arrays.stream(entries)
          .filter(entry -> {
            SftpATTRS attrs = entry.getAttrs();
            if (attrs.isDir()) {
              return false; // Es un directorio
            }
            Instant fileInstant = Instant.ofEpochSecond(attrs.getMTime());
            Duration fileAge = Duration.between(fileInstant, Instant.now());
            return fileAge.compareTo(time) > 0;
          })
          .map(LsEntry::getFilename)
          .collect(Collectors.toList());
    });

  }

  @Override
  public byte[] read(String filePath) throws StorageException {
    throw new UnsupportedOperationException("");
  }

  @Override
  public byte[] read(String filePath, String prefix) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public byte[] readByBucket(String bucketName, String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public InputStream readInputStream(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> listFiles() throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<Result<Item>> searchFilesByTags(Map<String, String> tags) {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public String generateLink(String filePath, int days) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public String generateLink(String filePath, int days, boolean isObfuscated) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void downloadLink(String etag, HttpServletRequest request, HttpServletResponse response)
      throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> listFiles(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> listFiles(String filePath, boolean isRecursive) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public boolean fileExist(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }



}

