package com.minsait.onesait.customer.lite.commons.storage.reader;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import io.minio.Result;
import io.minio.messages.Item;

public interface Reader {

  static final String ERR_READING_FILE = "Error reading file.";

  byte[] read(String filePath) throws StorageException;

  byte[] read(String filePath, String prefix) throws StorageException;

  byte[] readByBucket(String bucketName, String filePath) throws StorageException;

  InputStream readInputStream(String filePath) throws StorageException;

  List<String> listFiles() throws StorageException;

  List<Result<Item>> searchFilesByTags(Map<String, String> tags);

  String generateLink(String filePath, int days) throws StorageException;

  String generateLink(String filePath, int days, boolean isObfuscated) throws StorageException;

  void downloadLink(String etag, HttpServletRequest request, HttpServletResponse response) throws StorageException;

  List<String> listFiles(String filePath, boolean isRecursive) throws StorageException;

  List<String> listFiles(String filePath) throws StorageException;

  boolean fileExist(String filePath) throws StorageException;
}
