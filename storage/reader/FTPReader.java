package com.minsait.onesait.customer.lite.commons.storage.reader;

import java.time.Duration;
import java.util.List;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;

public interface FTPReader extends Reader {
  List<String> listFilesGT(String path, Duration time) throws StorageException;
}
