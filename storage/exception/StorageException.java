package com.minsait.onesait.customer.lite.commons.storage.exception;

public class StorageException
    extends Exception {
  private static final long serialVersionUID = 1L;

  public StorageException(String errorMessage, Throwable error) {
    super(errorMessage, error);
  }

  public StorageException(String errorMessage) {
    super(errorMessage);
  }
}
