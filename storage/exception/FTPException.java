package com.minsait.onesait.customer.lite.commons.storage.exception;

public class FTPException
    extends Exception {
  private static final long serialVersionUID = 1L;

  public FTPException(String errorMessage, Throwable error) {
    super(errorMessage, error);
  }

  public FTPException(String errorMessage) {
    super(errorMessage);
  }
}
