package com.minsait.onesait.customer.lite.commons.storage.writer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import com.minsait.onesait.customer.lite.commons.storage.commons.MinioCommon;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import com.minsait.onesait.customer.lite.commons.storage.interceptor.PrefixHolder;
import io.minio.BucketExistsArgs;
import io.minio.CopyObjectArgs;
import io.minio.CopySource;
import io.minio.DeleteObjectTagsArgs;
import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.MakeBucketArgs;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.MinioException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@Component
@ConditionalOnProperty(value = {"customer.storage.type"}, havingValue = "MINIO")
@Primary
public class MinioWriter extends MinioCommon implements Writer {

  public MinioWriter(@Value("${customer.storage.accessKey}") String accessKey,
      @Value("${customer.storage.secretKey}") String secretKey,
      @Value("${customer.storage.endpoint}") String endpoint,
      PrefixHolder prefixHolder) {
    super(accessKey, secretKey, endpoint, prefixHolder);
  }

  @Override
  public void write(byte[] bytes, String filePath) throws StorageException {
    write(bytes, filePath, Collections.emptyMap(), "");
  }
  
  /**
   * GAFEGA-121635 Write file in a specific bucket name
   * @param bucketName String bucket name
   * @param bytes byte[] byte file
   * @param filePath String file path
   * @throws StorageException storage exception
   */
  @Override
  public void write(String bucketName, byte[] bytes, String filePath) throws StorageException {
    write(bytes, filePath, Collections.emptyMap(), bucketName);
  }  

  @Override
  public void write(byte[] bytes, String filePath, Map<String, String> tags, String strBucketName) throws StorageException {
    try {
      String bucketName = strBucketName;
      if (bucketName == null || bucketName.isEmpty()) { // GAFEGA-121635 Write file in a specific bucket name
        bucketName = getBucketName(); 
      }

      if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
        minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
      }

      minioClient.putObject(PutObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .stream(new ByteArrayInputStream(bytes), bytes.length, -1)
          .tags(tags)
          .build());
    } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | InsufficientDataException
        | XmlParserException | InternalException | InvalidResponseException | ErrorResponseException
        | ServerException e) {
      throw new StorageException("Error al guardar el archivo en MinIO", e);
    }
  }

  @Override
  public void write(InputStream inputStream, String filePath) throws StorageException {
    write(inputStream, filePath, Collections.emptyMap());
  }  

  @Override
  public void write(InputStream inputStream, String filePath, Map<String, String> tags) throws StorageException {
    try {
      String bucketName = getBucketName();

      if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
        minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
      }

      minioClient.putObject(PutObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .stream(inputStream, inputStream.available(), -1)
          .tags(tags)
          .build());
    } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | InsufficientDataException
        | XmlParserException | InternalException | InvalidResponseException | ErrorResponseException
        | io.minio.errors.ServerException e) {
      throw new StorageException("Error al guardar el archivo en MinIO", e);
    }
  }

  @Override
  public void move(String sourcePath, String destinationPath) throws StorageException {
    String bucketName = getBucketName();

    try {
      minioClient.copyObject(CopyObjectArgs.builder()
          .source(CopySource.builder().bucket(bucketName).object(sourcePath).build())
          .bucket(bucketName)
          .object(destinationPath)
          .build());
      minioClient.removeObject(RemoveObjectArgs.builder()
          .bucket(bucketName)
          .object(sourcePath)
          .build());
    } catch (

        MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException("Error al mover el archivo: " + e.getMessage(), e);
    }
  }

  @Override
  public void copy(String sourcePath, String destinationPath) throws StorageException {
    try {
      String bucketName = getBucketName();

      minioClient.copyObject(CopyObjectArgs.builder()
          .source(CopySource.builder().bucket(bucketName).object(sourcePath).build())
          .bucket(bucketName)
          .object(destinationPath)
          .build());
    } catch (
        MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException("Error al mover el archivo: " + e.getMessage(), e);
    }
  }

  @Override
  public void delete(String filePath) throws StorageException {
    try {
      String bucketName = getBucketName();

      minioClient.removeObject(RemoveObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .build());
    } catch (
        MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException("Error al mover el archivo: " + e.getMessage(), e);
    }
  }

  @Override
  public void deleteTags(String filePath) throws StorageException {
    try {
      String bucketName = getBucketName();

      minioClient.deleteObjectTags(DeleteObjectTagsArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .build());
    } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException("Error al eliminar los tags: " + e.getMessage(), e);
    }
  }

  @Override
  public void updateTags(String filePath, Map<String, String> tags) throws StorageException {
    try {
      String bucketName = getBucketName();

      GetObjectResponse objectResponse = minioClient.getObject(GetObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .build());

      byte[] allBytes = IOUtils.toByteArray(objectResponse);
      InputStream inputStream = new ByteArrayInputStream(allBytes);

      minioClient.removeObject(RemoveObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .build());

      minioClient.putObject(PutObjectArgs.builder()
          .bucket(bucketName)
          .object(filePath)
          .stream(inputStream, allBytes.length, -1)
          .tags(tags)
          .build());
    } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
      throw new StorageException("Error al sobrescribir los tags: " + e.getMessage(), e);
    }
  }
}
