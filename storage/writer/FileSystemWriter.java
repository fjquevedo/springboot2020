package com.minsait.onesait.customer.lite.commons.storage.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import com.minsait.onesait.architecture.multitenant.context.MultiTenantContext;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import lombok.extern.slf4j.Slf4j;

@Component
@ConditionalOnProperty(value = {"customer.storage.type"}, havingValue = "FILE_SYSTEM", matchIfMissing = true)
@Slf4j
@Primary
public class FileSystemWriter implements Writer {

  @Value("${customer.storage.prefix:#{null}}")
  private Optional<String> prefix;

  @Override
  public void write(byte[] bytes, String filePath) throws StorageException {
    Path pathFinal = concatBaseDir(filePath);

    createParentDirectoriesIfNotExist(pathFinal);

    if (bytes == null) {
      log.warn("Bytes to write are null");
      return;
    }
    try {
      FileUtils.writeByteArrayToFile(new File(pathFinal.toString()), bytes);
    } catch (IOException e) {
      throw new StorageException("Error writing file " + pathFinal, e);
    }
  }

  @Override
  public void write(byte[] bytes, String filePath, Map<String, String> tags, String strBucketName) throws StorageException {
    throw new UnsupportedOperationException("Método no implementado");
  }
  
  /**
   * GAFEGA-121635 Write file in a specific bucket name
   * @param bucketName String bucket name
   * @param bytes byte[] byte file
   * @param filePath String file path
   * @throws StorageException storage exception
   */
  @Override
  public void write(String bucketName, byte[] bytes, String filePath) throws StorageException {
    throw new UnsupportedOperationException("Método no implementado");
  }   

  @Override
  public void write(InputStream inputStream, String filePath) throws StorageException {
    Path pathFinal = concatBaseDir(filePath);

    createParentDirectoriesIfNotExist(pathFinal);

    try (FileOutputStream outputStream = new FileOutputStream(pathFinal.toString())) {
      byte[] buffer = new byte[1024];
      int bytesRead;
      while ((bytesRead = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, bytesRead);
      }
    } catch (IOException e) {
      throw new StorageException("Error writing file " + pathFinal, e);
    }
  }

  @Override
  public void write(InputStream inputStream, String filePath, Map<String, String> tags) throws StorageException {
    throw new UnsupportedOperationException("Método no implementado");
  }

  @Override
  public void move(String sourcePath, String destinationPath) throws StorageException {
    sourcePath = concatBaseDir(sourcePath).toString();
    destinationPath = concatBaseDir(destinationPath).toString();

    createParentDirectoriesIfNotExist(Path.of(destinationPath));

    File source = new File(sourcePath);
    File target = new File(destinationPath);
    try {
      Files.move(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException ex) {
      throw new StorageException("Error moving file", ex);
    }
  }

  @Override
  public void copy(String sourcePath, String destinationPath) throws StorageException {
    sourcePath = concatBaseDir(sourcePath).toString();
    destinationPath = concatBaseDir(destinationPath).toString();

    createParentDirectoriesIfNotExist(Path.of(destinationPath));

    File source = new File(sourcePath);
    File target = new File(destinationPath);
    try {
      Files.copy(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException ex) {
      throw new StorageException("Error copying file", ex);
    }
  }

  @Override
  public void delete(String filePath) throws StorageException {
    filePath = concatBaseDir(filePath).toString();
    File source = new File(filePath);
    try {
      Files.delete(source.toPath());
    } catch (IOException ex) {
      throw new StorageException("Error deleting file", ex);
    }
  }

  @Override
  public void deleteTags(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Método no implementado");
  }

  @Override
  public void updateTags(String filePath, Map<String, String> tags) throws StorageException {
    throw new UnsupportedOperationException("Método no implementado");
  }

  private Path concatBaseDir(String path) {
    String baseDir;

    if (prefix.isPresent() && !prefix.get().isEmpty()) {
      baseDir = "/" + prefix.get() + "-" + MultiTenantContext.getTenantId();
    } else {
      baseDir = "/" + MultiTenantContext.getTenantId();
    }
    baseDir = baseDir.toLowerCase();

    String fullPath = StringUtils.startsWith(path, "/") ? path : "/".concat(path);
    return Paths.get(baseDir, fullPath);
  }

  private void createParentDirectoriesIfNotExist(Path path) throws StorageException {
    Path parentPath = path.getParent();
    if (!Files.exists(parentPath)) {
      try {
        Files.createDirectories(parentPath);
      } catch (NoSuchFileException ex) {
        throw new StorageException("Error creating parent directories: " + ex.getMessage(), ex);
      } catch (IOException ex) {
        throw new StorageException("Error creating parent directories", ex);
      }
    }
  }
}
