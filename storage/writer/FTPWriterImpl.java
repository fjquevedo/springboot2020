package com.minsait.onesait.customer.lite.commons.storage.writer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.minsait.onesait.architecture.multitenant.context.MultiTenantContext;
import com.minsait.onesait.customer.lite.commons.storage.exception.FTPException;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import com.minsait.onesait.customer.lite.commons.storage.ftp.FTPClientAutoCloseable;
import com.minsait.onesait.customer.lite.commons.storage.ftp.FTPConnectionPoolManager;

@Component("ftpWriter")
public class FTPWriterImpl implements FTPWriter {

  @Autowired
  private FTPConnectionPoolManager ftpPoolManager;

  @Override
  public byte[] readFromExternal(String location, Map<String, Object> parameters) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public InputStream readInputStreamFromExternal(String location, Map<String, Object> parameters)
      throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void write(byte[] bytes, String filePath) throws StorageException {
    InputStream stream = new ByteArrayInputStream(bytes);
    write(stream, filePath);
  }

  @Override
  public void write(byte[] bytes, String filePath, Map<String, String> tags, String strBucketName)
      throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void write(String bucketName, byte[] bytes, String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void write(InputStream inputStream, String filePath) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();
    FTPClient ftpClient = null;
    try (FTPClientAutoCloseable ftpClientAutoCloseable = ftpPoolManager.getConnection(tenantId)) {
      ftpClient = ftpClientAutoCloseable.getClient();
      if (!ftpClient.storeFile(filePath, inputStream)) {
        throw new StorageException("Exception writing remote file " + filePath);
      }
    } catch (IOException | StorageException | FTPException ex) {
      throw new StorageException("Exception writing remote file", ex);
    }
  }

  @Override
  public void write(InputStream inputStream, String filePath, Map<String, String> tags) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void move(String sourcePath, String destinationPath) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();
    FTPClient ftpClient = null;

    try (FTPClientAutoCloseable ftpClientAutoCloseable = ftpPoolManager.getConnection(tenantId)) {
      ftpClient = ftpClientAutoCloseable.getClient();
      try {
        if (!ftpClient.rename(sourcePath, destinationPath)) {
          throw new StorageException("No se pudo mover el archivo de " + sourcePath + " a " + destinationPath);
        }
      } catch (IOException | StorageException ex) {
        throw new StorageException("Exception ranaming remote file", ex);
      }
    } catch (FTPException ex) {
      throw new StorageException("Exception getting ftp connection", ex);
    }
  }

  @Override
  public void copy(String sourcePath, String destinationPath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void delete(String filePath) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();
    FTPClient ftpClient = null;
    try (FTPClientAutoCloseable ftpClientAutoCloseable = ftpPoolManager.getConnection(tenantId)) {
      ftpClient = ftpClientAutoCloseable.getClient();
      if (!ftpClient.deleteFile(filePath)) {
        throw new StorageException("Exception delete remote file " + filePath);
      }
    } catch (IOException | StorageException | FTPException ex) {
      throw new StorageException("Exception delete remote file", ex);
    }
  }

  @Override
  public void deleteTags(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void updateTags(String filePath, Map<String, String> tags) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

}
