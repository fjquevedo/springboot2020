package com.minsait.onesait.customer.lite.commons.storage.writer;

import java.io.InputStream;
import java.util.Map;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;

public interface Writer {

  void write(byte[] bytes, String filePath) throws StorageException;

  void write(byte[] bytes, String filePath, Map<String, String> tags, String strBucketName) throws StorageException;

  void write(InputStream inputStream, String filePath) throws StorageException;

  void write(InputStream inputStream, String filePath, Map<String, String> tags) throws StorageException;

  void write(String destinationPath, byte[] bytes, String filePath) throws StorageException;

  /**
   * Move or rename a file to a target file.
   * 
   * @param sourcePath the path to the file to move
   * @param destinationPath the path to the target file
   * @throws StorageException generic storageException
   */
  void move(String sourcePath, String destinationPath) throws StorageException;

  /**
   * Copy a file to a target file.
   * 
   * @param sourcePath the path to the file to move
   * @param destinationPath the path to the target file
   * @throws StorageException generic storageException
   */
  void copy(String sourcePath, String destinationPath) throws StorageException;

  void delete(String filePath) throws StorageException;

  void deleteTags(String filePath) throws StorageException;

  void updateTags(String filePath, Map<String, String> tags) throws StorageException;
}
