package com.minsait.onesait.customer.lite.commons.storage.writer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.stereotype.Component;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.minsait.onesait.architecture.multitenant.context.MultiTenantContext;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;
import com.minsait.onesait.customer.lite.commons.storage.sftp.SFTPTenantConfiguration;
import lombok.extern.slf4j.Slf4j;

@Component("sftpWriter")
@Slf4j
public class SFTPWriterImpl implements FTPWriter {

  @Autowired
  private SFTPTenantConfiguration multitenantConfig;

  @Override
  public byte[] readFromExternal(String location, Map<String, Object> parameters) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public InputStream readInputStreamFromExternal(String location, Map<String, Object> parameters)
      throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void write(byte[] bytes, String filePath) throws StorageException {
    InputStream stream = new ByteArrayInputStream(bytes);
    write(stream, filePath);
  }

  @Override
  public void write(byte[] bytes, String filePath, Map<String, String> tags, String strBucketName)
      throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void write(String bucketName, byte[] bytes, String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void write(InputStream inputStream, String filePath) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();

    String filePathDirectory = filePath.substring(0, filePath.lastIndexOf('/'));

    SessionFactory<LsEntry> sessionFactoryForTenant = multitenantConfig.getSessionFactoryForTenant(tenantId);
    SftpRemoteFileTemplate template = new SftpRemoteFileTemplate(sessionFactoryForTenant);
    try {
      template.execute(session -> {

        if (!session.exists(filePathDirectory)) {
          makeDirectory(filePathDirectory, session);
        }

        session.write(inputStream, filePath);
        log.info("Success writing file to remote host sftp.");
        return null;
      });
    } catch (Exception e) {
      throw new StorageException("Error writing file to SFTP server.", e);
    }

  }

  @Override
  public void write(InputStream inputStream, String filePath, Map<String, String> tags) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void move(String sourcePath, String destinationPath) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();

    SessionFactory<LsEntry> sessionFactoryForTenant = multitenantConfig.getSessionFactoryForTenant(tenantId);
    SftpRemoteFileTemplate template = new SftpRemoteFileTemplate(sessionFactoryForTenant);

    try {
      template.execute(session -> {
        session.rename(sourcePath, destinationPath);
        return null;
      });

    } catch (Exception e) {
      throw new StorageException("Error moving file to SFTP server.", e);
    }
  }

  @Override
  public void copy(String sourcePath, String destinationPath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void delete(String filePath) throws StorageException {
    String tenantId = MultiTenantContext.getTenantId();

    SessionFactory<LsEntry> sessionFactoryForTenant = multitenantConfig.getSessionFactoryForTenant(tenantId);
    SftpRemoteFileTemplate template = new SftpRemoteFileTemplate(sessionFactoryForTenant);

    try {
      template.execute(session -> {
        session.remove(filePath);
        return null;
      });

    } catch (Exception e) {
      throw new StorageException("Error removing file to SFTP server.", e);
    }
  }

  @Override
  public void deleteTags(String filePath) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void updateTags(String filePath, Map<String, String> tags) throws StorageException {
    throw new UnsupportedOperationException("Not implemented");
  }

  private void makeDirectory(String filePathDirectory, Session<LsEntry> session) throws IOException {
    String filePathDirectoryAux = filePathDirectory.substring(0, filePathDirectory.lastIndexOf('/'));
    if (session.exists(filePathDirectoryAux)) {
      session.mkdir(filePathDirectory);
    } else {
      makeDirectory(filePathDirectoryAux, session);
      session.mkdir(filePathDirectory);
    }
  }

}
