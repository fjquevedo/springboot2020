package com.minsait.onesait.customer.lite.commons.storage.writer;

import java.io.InputStream;
import java.util.Map;
import com.minsait.onesait.customer.lite.commons.storage.exception.StorageException;

public interface FTPWriter extends Writer {
  byte[] readFromExternal(String location, Map<String, Object> parameters) throws StorageException;

  InputStream readInputStreamFromExternal(String location, Map<String, Object> parameters) throws StorageException;
}
