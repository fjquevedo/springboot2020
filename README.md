# springboot2020

## URI versioning - TWITTER
https://developer.twitter.com/en/docs/twitter-api/versioning

* http://localhost:8080/v1/person
* http://localhost:8080/v2/person


## Request Parameter versioning - AMAZON
* http://localhost:8080/person?version=1
* http://localhost:8080/person?version=2

## (Custom) Header versioning - MICROSOFT
https://github.com/microsoft/api-guidelines/blob/vNext/azure/Guidelines.md
* SAME-URL headers=[X-API-VERSION=1]
* SAME-URL headers=[X-API-VERSION=2]

## (Custom) Header versioning - GITHUB
https://docs.github.com/en/rest/about-the-rest-api/api-versions?apiVersion=2022-11-28
* SAME-URL produces= application/vnd.company.app-v1+json
* SAME-URL produces= application/vnd.company.app-v2+json

Problemas:
URI POLLUTION
MISSUSE OF HTTP HEADERS
CACHING
Can we execute request on browsers
API Documentation


Number one is URI pollution.
When you look at URI versioning and request parameter versioning, what we're doing is we're creating new URLs to represent the new version.

So there is a lot of URI pollution when it comes to URI versioning
and request parameter versioning.

However, in the case of header's versioning and media type versioning, we're using the same URL so they have less amount of URI pollution.

The other factor to consider is misuse of HTTP headers.
HTTP headers were never meant to be used for versioning.

So headers versioning and media type versioning misuse the HTTP headers.

The third factor is caching.

Typically, caching is done based on the URL.

And when it comes to header versioning and media type versioning, we're using different versions.

However, both those versions can have the same URL.

So when it comes to header versioning and media type versioning,

you cannot cash just based on the URL.

You also need to look at the headers before you do caching.

Another important factor is can we execute the request on the browser?

When it comes to URL versioning and request parameter versioning you can easily execute them on the browser because the differentiation is in the URL.

However, when it comes to headers versioning and media versioning, the differentiation is in the headers.

Typically, you need to have a command line utility or you need to make use of a REST API client to be able to differentiate based on the headers.

And the other factor we would need to consider is API documentation.
Typically, generating API documentation for URI versioning and request parameter versioning is easy because the URLs are different for both the versions.

Typical API documentation generation tools might not support generating documentation differentiating based on the headers.

So generating documentation for headers versioning and media type versioning might be a little difficult.

So as we're discussing right now, there is no perfect solution when it comes to versioning.

You can see that each of these versioning approaches are being used by different large enterprises.

URI versioning is used by Twitter,
request parameter versioning is used in Amazon.
Header versioning is used in Microsoft
and media type versioning is used in GitHub.

So you can see that a lot of different enterprises are making use of different ways to version your REST API.

My recommendations when it comes to versioning  is to think about versioning even before you need it.

When you're starting to build your REST API, that's when you'd think about versioning.

And number two is to have a consistent versioning approach across your enterprise.

The best practice always is one enterprise, one versioning approach across your multiple different projects, multiple applications.

You'd want one way to version yous REST API. It does not matter which approach you choose, but it's very, very important that the approach used is consistent across all projects and all applications in an enterprise.
