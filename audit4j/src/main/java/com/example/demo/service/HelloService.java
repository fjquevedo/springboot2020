package com.example.demo.service;

import org.audit4j.core.annotation.Audit;
import org.audit4j.core.annotation.AuditField;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    @Audit(action = "HelloService.hello")
    public String hello(@AuditField(field="loggedInUsername") String name) {
        return "Welcome "  + name;
    }
}
