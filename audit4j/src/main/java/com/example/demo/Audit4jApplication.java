package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Audit4jApplication {

	public static void main(String[] args) {
		SpringApplication.run(Audit4jApplication.class, args);
	}

}
