public class Expression {
    private String expression;

    public Expression(String expression) {
        this.expression = expression;
    }

    public String getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return expression;
    }

    // Métodos auxiliares para AND, OR, EQ, NEQ
    public static Expression and(Expression left, Expression right) {
        return new Expression(left.toString() + " AND " + right.toString());
    }

    public static Expression or(Expression left, Expression right) {
        return new Expression(left.toString() + " OR " + right.toString());
    }

    public static Expression eq(String column, String param) {
        return new Expression(column + " = " + param);
    }

    public static Expression neq(String column, String param) {
        return new Expression(column + " != " + param);
    }
}