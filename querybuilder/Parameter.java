import java.util.HashMap;
import java.util.Map;

public class Parameter {
    private Map<String, Object> parameters = new HashMap<>();

    public void addParameter(String name, Object value) {
        parameters.put(name, value);
    }

    public Object getParameter(String name) {
        return parameters.get(name);
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }
}