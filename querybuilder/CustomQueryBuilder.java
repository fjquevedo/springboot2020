import java.util.Map;

public class CustomQueryBuilder {

    private StringBuilder query;
    private static final String NEW_LINE = System.lineSeparator();
    private static final String INDENT = "  ";
    private Parameter parameters;

    private CustomQueryBuilder() {
        query = new StringBuilder();
        parameters = new Parameter();
    }

    public static CustomQueryBuilder create() {
        return new CustomQueryBuilder();
    }

    public CustomQueryBuilder select(String... columns) {
    if (columns == null || columns.length == 0) {
        throw new IllegalArgumentException("At least one column must be specified");
    }
    query.append("SELECT ")
         .append(String.join(", ", columns));
    return this;
}

    public CustomQueryBuilder from(Table table) {
        query.append(" FROM ").append(table.toString());
        return this;
    }

    public CustomQueryBuilder join(Table table, Expression expression, String type) {
        query.append(" ").append(type).append(" JOIN ").append(table.toString())
                .append(" ON (").append(expression.toString()).append(")");
        return this;
    }

    public CustomQueryBuilder where(Expression expression) {
        query.append(" WHERE (").append(expression.toString()).append(")");
        return this;
    }

    public CustomQueryBuilder setParameter(String name, Object value) {
        parameters.addParameter(name, value);
        return this;
    }

    public String build() {
        String builtQuery = query.toString();
        for (Map.Entry<String, Object> entry : parameters.getParameters().entrySet()) {
            builtQuery = builtQuery.replace(":" + entry.getKey(), entry.getValue().toString());
        }
        return builtQuery;
    }
}
