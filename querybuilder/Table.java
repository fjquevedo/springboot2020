import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Table {
    private final String name;
    private final String alias;
    @Override
    public String toString() {
        return name + " " + alias;
    }
}