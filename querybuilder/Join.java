
public class Join {
    private final Table leftTable;
    private final Table rightTable;
    private final String leftColumn;
    private final String rightColumn;
    private final String joinType; // INNER, LEFT, RIGHT, etc.

    // Constructor, getters y setters
    public Join(Table leftTable, Table rightTable, String leftColumn, String rightColumn, String joinType) {
        this.leftTable = leftTable;
        this.rightTable = rightTable;
        this.leftColumn = leftColumn;
        this.rightColumn = rightColumn;
        this.joinType = joinType;
    }

    public Table getLeftTable() {
        return leftTable;
    }

    public Table getRightTable() {
        return rightTable;
    }

    public String getLeftColumn() {
        return leftColumn;
    }

    public String getRightColumn() {
        return rightColumn;
    }

    public String getJoinType() {
        return joinType;
    }
}
