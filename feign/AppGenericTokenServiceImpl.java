import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.minsait.onesait.customer.lite.commons.security.config.oauth.BatchCredentialsConfig.BatchCredentials;
import com.minsait.onesait.customer.lite.commons.security.exception.InvalidTokenException;
import com.minsait.onesait.customer.lite.commons.security.model.OauthAuthentication;
import com.nimbusds.jose.shaded.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AppGenericTokenServiceImpl implements AppGenericTokenService {

  @Autowired
  private BatchCredentialsConfig batchCredentialsConfig;

  @Value("${tenant.service:}/api/v1/internal/login")
  private String tenantService;

  private RestTemplate loginRestTemplate;

  @PostConstruct
  public void init() {
    loginRestTemplate = new RestTemplate();
  }

  @Override
  public String getAppGenericToken(String tenant) {
    String token = null;
    String[] user = {"", ""};

    // Obtenemos el usuario batch del tenant
    List<BatchCredentials> listBatchCredentials = batchCredentialsConfig.getBatchCredentials();

    if (tenant != null && !tenant.isBlank() && listBatchCredentials != null) {
      getUser(user, tenant, listBatchCredentials);
      if (!user[0].isBlank()) {
        token = login(user, tenant);
      }
    }

    return token;
  }


  private String login(String[] user, String client) throws InvalidTokenException {

    String token = null;
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    JSONObject loginJsonObject = new JSONObject();
    loginJsonObject.put("username", user[0]);
    loginJsonObject.put("password", user[1]);
    loginJsonObject.put("clientName", client);

    HttpEntity<String> request =
        new HttpEntity<>(loginJsonObject.toString(), headers);

    ResponseEntity<OauthAuthentication> auth;

    try {
      log.debug("Post request {} to URL {}", request, tenantService);
      auth = this.loginRestTemplate.postForEntity(tenantService, request, OauthAuthentication.class);
    } catch (RestClientException e) {
      log.error("Problem with loggingRestTemplate ", e);
      throw e;
    } catch (Exception ex) {
      log.error("Error no controlado {}", ex);
      throw ex;
    }

    OauthAuthentication authBody = auth.getBody();

    if (authBody != null && authBody.accessToken != null && !"".equals(authBody.accessToken)) {
      log.debug("Process login response");
      token = authBody.accessToken;
    } else {
      throw new InvalidTokenException("User information and authentication token could not be retrieved");

    }
    return token;
  }

  private void getUser(String[] user, String client, List<BatchCredentials> listBatchCredentials) {
    for (BatchCredentials batchCredentials : listBatchCredentials) {
      if (batchCredentials.getClient().equalsIgnoreCase(client)) {
        user[0] = batchCredentials.getUser();
        user[1] = batchCredentials.getPassword();
        break;
      }
    }
  }


}
