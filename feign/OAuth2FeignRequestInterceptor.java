import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.minsait.onesait.customer.lite.commons.security.utils.SecurityConstants;
import com.minsait.onesait.customer.lite.commons.security.utils.SecurityUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OAuth2FeignRequestInterceptor implements RequestInterceptor {

  @Autowired
  private AppGenericTokenService appGenericTokenService;

  @Override
  public void apply(RequestTemplate template) {
    if (template.headers().containsKey(SecurityConstants.AUTHORIZATIONNAME) || 
        template.headers().containsKey(SecurityConstants.OP_APIKEY)) {
      log.trace("The authorization token has been already set ");
    } else {
      try {
        log.trace("Request with current token context.");
        String token = SecurityUtils.getBearerToken();
        if (token == null) {
          log.trace("Generate generic token.");
          // Obtenemos el tenant de la cabecera
          String client = "";
          if (template.headers().containsKey(SecurityConstants.XTENANTID_HEADER)) {
            client = template.headers().get(SecurityConstants.XTENANTID_HEADER).stream().findFirst()
                .orElse(null);
            token = appGenericTokenService.getAppGenericToken(client);
            if (token != null) {
              token = "Bearer " + token;
            }
          }
        }
        if (token != null) {
          template.removeHeader(SecurityConstants.AUTHORIZATIONNAME);
          template.header(SecurityConstants.AUTHORIZATIONNAME, token);
          log.trace("Authorization header added.");
        }
      } catch (Exception e) {
        log.error("Login failed {}", e.getMessage());
      }
    }
  }

}
