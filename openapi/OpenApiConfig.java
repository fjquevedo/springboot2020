
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
@Profile({"authNZ"})
public class OpenApiConfig {

  protected static final String HTTPS = "https";

  protected Set<String> protocols = new HashSet<>(Arrays.asList(HTTPS));

  @Value("${clite.host:}")
  protected String host;

  @Value("${server.servlet.context-path}")
  String contextPath;

  private static final String OAUTH = "OAUTH2";

  @Bean
  public OpenAPI customOpenAPI() {
    return new OpenAPI()
        .components(new Components()
            .addSecuritySchemes(OAUTH, apiKeySecuritySchema())) // define the apiKey SecuritySchema
        .info(new Info().title("Application RestAPI")
            .description("RESTful services documentation with OpenAPI 3.").version("1.0"))
        .security(Collections.singletonList(new SecurityRequirement().addList(OAUTH)))
        .addServersItem(new Server().url(host + contextPath));
  }

  public SecurityScheme apiKeySecuritySchema() {
    return new SecurityScheme()
        .name("Authorization")
        .description("Description about the TOKEN")
        .in(SecurityScheme.In.HEADER)
        .type(SecurityScheme.Type.HTTP)
        .scheme("Bearer");
  }
