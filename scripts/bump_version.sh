#!/bin/bash

# Archivo JSON
FILE="package.json"

# Obtener la versión actual utilizando awk
CURRENT_VERSION=$(awk -F\" '/"version":/ {print $4}' $FILE)

# Mostrar la versión actual
echo "La versión actual es: $CURRENT_VERSION"

# Pedir la nueva versión al usuario
read -p "Introduce la nueva versión: " NEW_VERSION

# Reemplazar la versión en el archivo utilizando sed
sed -i "s/\"version\": \"$CURRENT_VERSION\"/\"version\": \"$NEW_VERSION\"/" $FILE

# Confirmar el cambio
echo "La versión ha sido actualizada a: $NEW_VERSION"
