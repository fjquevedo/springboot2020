@echo off
setlocal enabledelayedexpansion

:: Variables
set "TOMCAT_DIR=C:\tomcat"
set "CONTEXT_DIR=C:\tomcat-context"
set "BACKUP_DIR=C:\backup"
set "DEPLOY_DIR=C:\deploy"
set "PROJECT_NAME=proyectox"
set "WAR_FILE=%DEPLOY_DIR%\%PROJECT_NAME%.war"
set "DATE_TIME=%date:~-4%_%date:~3,2%_%date:~0,2%_%time:~0,2%_%time:~3,2%"

:: Crear carpeta de backup con la fecha y hora actual
set "BACKUP_FOLDER=%BACKUP_DIR%\backup_%DATE_TIME%"
mkdir "%BACKUP_FOLDER%"
echo Directorio de Backup %BACKUP_FOLDER%

:: Copiar el archivo .war desplegado en Tomcat a la carpeta de backup
copy "%TOMCAT_DIR%\webapps\%PROJECT_NAME%.war" "%BACKUP_FOLDER%"
echo backup realizado

:: Eliminar el archivo .war desplegado y esperar unos segundos
del "%TOMCAT_DIR%\webapps\%PROJECT_NAME%.war"
echo despliegue elminado, esperando 10 segundos
timeout /t 10

:: Restaurar el archivo de contexto
copy "%CONTEXT_DIR%\%PROJECT_NAME%.xml" "%TOMCAT_DIR%\conf\Catalina\localhost\%PROJECT_NAME%.xml"
echo Fichero de contexto copiado %PROJECT_NAME%.xml

:: Mover el nuevo archivo .war a Tomcat
move "%WAR_FILE%" "%TOMCAT_DIR%\webapps\"

:: Mensaje de confirmación
echo Despliegue completado. La nueva version de %PROJECT_NAME%.war ha sido desplegada.
