#!/bin/bash

# Nombre del archivo JSON
FILE="angular.json"

# Función para obtener la versión actual
get_current_version() {
  awk -F\" '/"version":/ {print $4}' $FILE
}

# Función para actualizar la versión en el archivo JSON
update_version() {
  CURRENT_VERSION=$(get_current_version)
  echo "La versión actual es: $CURRENT_VERSION"
  read -p "Introduce la nueva versión: " NEW_VERSION
  sed -i "s/\"version\": \"$CURRENT_VERSION\"/\"version\": \"$NEW_VERSION\"/" $FILE
  echo "La versión ha sido actualizada a: $NEW_VERSION"
}

# Asegurarse de que estamos en la rama develop
git checkout develop

# Actualizar la rama develop con lo último del remoto
git pull origin develop

# Comprobar si hubo conflictos
if [[ $(git ls-files -u | wc -l) -ne 0 ]]; then
  echo "Existen conflictos de fusión. Resuélvalos antes de continuar."
  exit 1
else
  echo "No hay conflictos. Continuando con la construcción del proyecto..."
fi

# Construir el proyecto
npm install
ng build

# Incrementar la versión
update_version

# Obtener la nueva versión
NEW_VERSION=$(get_current_version)

# Crear una nueva rama de release
RELEASE_BRANCH="release/v$NEW_VERSION"
git checkout -b $RELEASE_BRANCH

# Añadir el archivo angular.json, hacer commit y push
git add $FILE
git commit -m "build(arqui): subir version $NEW_VERSION"
git push origin $RELEASE_BRANCH

echo "La nueva rama de release $RELEASE_BRANCH ha sido creada y empujada a origin."

# Volver a la rama develop
git checkout develop

# Borrar la rama de release en local
git branch -d release/v$NEW_VERSION
